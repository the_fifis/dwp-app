import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Grid, TextField, Button, Typography } from '@mui/material';
import { useState } from 'react';
import axiosClient from '../../config/axiosClient';

const ForgotPassword = () => {
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');

    const handleRecoverPass = () => {
        axiosClient.post('http://localhost:8080/recuperar-password', { email })
        .then(res => {
            console.log(res.data);
            setMessage(res.data.message);
        })
        .catch(error => {
            console.log(error);
            setMessage(error.message || '500 Error interno');
        })
    }

    if(message) {
        return(
            <Grid container component="main">
                <Grid item xs={12} sx={{ textAlign: 'center' }}>
                   <Typography variant="body2">
                        {message}
                    </Typography> 
                </Grid>
            </Grid>
        )
    }

    return (
        <Grid container component="main">
           <Grid item xs={12} sx={{ textAlign: 'center'}}>
               <TextField
                    name="email"
                    label="Email de cuenta a recuperar"
                    autoFocus
                    value={email}
                    sx={{width: '250px'}}
                    onChange={e => setEmail(e.target.value)}
                /> 
            </Grid> 
                <Grid item xs={12} sx={{ textAlign: 'center', mt: 2}}>
               <Button
               variant="contained"
               endIcon={<FontAwesomeIcon icon={faPaperPlane} pull="right" beat />}
               onClick={handleRecoverPass}
               >Recuperar contraseña</Button> 
            </Grid> 
        </Grid>
    );
};

export default ForgotPassword;