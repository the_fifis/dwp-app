import { faSave } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Grid, TextField, Button, Typography } from '@mui/material';
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axiosClient from '../../config/axiosClient';

const NewPassword = () => {
    const [password, setPassword] = useState('');
    const [isValidToken, setIsValidToken] = useState('');
    const [message, setMessage] = useState('');
    const [showForm, setShowForm] = useState(false);
    
    const {token} = useParams();

    const handleSavePass = () => {
        if(password.length > 8) {
            axiosClient.post('http://localhost:8080/actualizar-password', {token, password})
            .then(res => {
                console.log(res.data);
                setMessage(res.data.message);
                setShowForm(false);
            })
            .catch(error => {
                console.log(error);
                setMessage(error.message || 'Error interno');
            })
        }
    }

    useEffect(() => {
        if(token) {
            axiosClient.post('http://localhost:8080/validar-token', {token})
            .then(res => {
                console.log(res.data);
                setIsValidToken(res.data.isValid)
                setShowForm(res.data.isValid);
            })
            .catch(error => {
                console.log(error);
                setMessage(error.message || 'Error interno');
            })
        }
    }, [token])

    if(isValidToken && showForm) {
        return(
            <Grid container component="main">
               <Grid items xs={12} sx={{ textAlign: 'center' }}>
                   <TextField
                    name="password"
                    label="Nueva contraseña"
                    autoFocus
                    value={password}
                    type="password"
                    sx={{width: '250px'}}
                    onChange={e=> setPassword(e.target.value)}
                   />
                </Grid> 
                <Grid item xs={12} sx={{ textAlign: 'center', mt: 2}}>
               <Button
               variant="contained"
               endIcon={<FontAwesomeIcon icon={faSave} pull="right" beat />}
               onClick={handleSavePass}
               >Guardar nueva contraseña</Button> 
            </Grid> 
            </Grid>
        );
    }

    return(
        <Grid container component="main">
            <Grid item xs={12} sx={{ textAlign: 'center' }}>
               <Typography variant="body2">
                    {message}
                </Typography> 
            </Grid>
        </Grid>
    );
};

export default NewPassword;