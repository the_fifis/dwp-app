import { Button, TextField } from "@mui/material";
import Paper from "@mui/material/Paper";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Grid from '@mui/material/Grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Dialog from '@mui/material/Dialog';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';


import axiosClient from '../../config/axiosClient';
import { useState, useEffect, useRef } from "react";
import { faPencil, faTrash } from "@fortawesome/free-solid-svg-icons";

const Template = () => {
    const [templates, setTemplates] = useState([]);  
    const [categories, setCategories] = useState([]);  
    const [category, setCategory] = useState([]);  
    const [image, setImage] = useState([]);
    const [template, setTemplate] = useState({preview: 'https://img.bfmtv.com/c/630/420/871/7b9f41477da5f240b24bd67216dd7.jpg'});
    const [edit, setEdit] = useState(false);
    const [dialog, setDialog] = useState([{open: false, status: false, title: '', msg: ''}]);
    const [dialogCategory, setDialogCategory] = useState(false);
    const hiddenFileInput = useRef(null);
      
    const allTemplates = () => {
        axiosClient.get(`http://localhost:8080/templates`)
        .then((res) => {
            setTemplates([...res.data.resultados]);
        })
        .catch((error) => {
            console.log(error)
        });
    }  

    const allCategories = () => {
        axiosClient.get(`http://localhost:8080/categories`)
        .then((res) => {
            setCategories([...res.data.resultados])
        })
        .catch((error) => {
            console.log(error)
        });
    }  

    const handleCancel = (e) => {
        const emptyArray = {name: '', description: '', price : ''};
        setTemplate({...emptyArray, preview: 'https://img.bfmtv.com/c/630/420/871/7b9f41477da5f240b24bd67216dd7.jpg'});
        setImage([]);
    }

    const handleChange = (e) => {
        setTemplate({...template, [e.target.name]: e.target.value});
    }

    const handleChangeCategory = (e) => {
        setCategory({...category, [e.target.name]: e.target.value});
    }

    const handleClickPreviewImage = event => {
        hiddenFileInput.current.click();
    };

    const handleDeleteCategory = (e) => {
        axiosClient.post(`http://localhost:8080/category/delete`, {id: template.category})
        .then((res) => {
            console.log(res.status);
            if(res.status == 403) {
                setDialog({open: true, status: false, msg: 'No tienes permisos'});
            } else {
                setDialog({open: true, status: true, msg: 'Usuario eliminado con éxito'});
            }
        })
        .catch(function (error) {
            if (error.response.status === 403) {
                setDialog({open: true, status: false, msg: 'No tienes permisos'});
            } 
            else {
                setDialog({open: true, status: true, msg: error.response.data});
            } 
        }); 
    }
    
    const handleDelete = (e) => {
        axiosClient.post(`http://localhost:8080/template/remove`, {id: e.currentTarget.value})
        .then((res) => {
            setDialog({open: true, status: true, msg: 'Plantilla eliminado con éxito'});
        })
        .catch(function (error) {
            if (error.response) {
                setDialog({open: true, status: true, title: 'Eliminado', msg: error.response.data});
            } 
        }); 
        setDialog({open: true, status: false, msg: 'No tienes permisos'});
    }
    
    const handleDialogCategoryOpen = () => {
        setDialogCategory(true);
    };

    const handleDialogCategoryClose = () => {
        setDialogCategory(false);
    };

    const handleDialogClose = () => {
        setDialog({...dialog, open: false});
    };

    const handleFill = (e) => {
        setEdit(true);
        axiosClient.get(`http://localhost:8080/template?q=${e.currentTarget.value}`)
        .then((res) => {
            setTemplate({...res.data.resultados[0], preview: res.data.resultados[0].image});
        })
        .catch((error) => {
            setDialog({open: true, status: false, title: 'Error', msg: error});
        });
    }

    const handleSelectImage = (e) => {   
        setTemplate({...template, preview: URL.createObjectURL(e.target.files[0]), image: 'http://localhost/sai/assets/'+e.target.files[0].name});
        setImage(e.target.files[0]);
    }

    const handleSelectCategory = (e) => {
        setTemplate({...template, category: e.target.value});
    }

    const handleSave = (e) => {
        if(template.name && template.description && template.image && template.price) {
            const formData = new FormData(); 
        
            // Update the formData object 
            formData.append( 
            "myFile", 
            image, 
            image.name
            ); 
            axiosClient.post("http://localhost/sai/scripts/upload.php", formData).catch(function (error) {
                if (error.response) {
                    setDialog({open: true, status: false, title: 'Error', msg: error.response.data});
                }
            }); 
            if(!edit) {
                axiosClient.post(`http://localhost:8080/templates`, template).catch(function (error) {
                    if (error.response) {
                        setDialog({open: true, status: false, title: 'Error', msg: error.response.data});
                    } 
                }); 

                setDialog({open: true, status: true, title: 'Guardado', msg: 'Plantilla guardada con éxito'});
                setEdit(false);

            } else {
                axiosClient.post(`http://localhost:8080/template/update`, template).catch(function (error) {
                    if (error.response) {
                        setDialog({open: true, status: false, title: 'Error', msg: error.response.data});
                    } 
                }); 

                setDialog({open: true, status: true, title: 'Guardad', msg: 'Cambios guardados con éxito'});
                setEdit(false);
            }
        } else {
            setDialog({open: true, status: false, title: 'Error', msg: 'Debes llenar todos los campos'});
        }
        
    }

    const handleSaveCategory = (e) => {
        if(category.title && category.description) {
            axiosClient.post(`http://localhost:8080/category/add`, category).catch(function (error) {
                if (error.response) {
                    setDialog({open: true, status: false, msg: error.response.data});
                } 
            }); 
            setDialog({open: true, status: true, title: 'Guardado', msg: 'Cambios guardados con éxito'});
            setDialogCategory(false);
        } else {
            setDialog({open: true, status: false, title: 'Error', msg: 'Debes llenar todos los campos'});
        }
    }
      
    useEffect(() => {
        allTemplates();
        allCategories();
  }, [dialog.open]
)

    return (
        <Paper
        sx={{
          p: 3,
          minHeight: '75vh',
        }}
      >
          <Dialog
            open={dialog.open}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            >
            {!dialog.status && (<Alert variant="filled" severity="warning" 
            onClose={handleDialogClose}>  
            <AlertTitle>{dialog.title}</AlertTitle>
            {dialog.msg}
            </Alert>)}
            {dialog.status && (<Alert variant="filled" severity="success"
            onClose={handleDialogClose}>    
            <AlertTitle>{dialog.title}</AlertTitle>
            {dialog.msg}
            </Alert>)}
          </Dialog>

          <Dialog open={dialogCategory} onClose={handleDialogCategoryClose}>
            <DialogTitle>Crear categoría</DialogTitle>
            <DialogContent>
            <DialogContentText>
                Agrega un título y descripción de la categoría.
            </DialogContentText>
            <TextField
                autoFocus
                margin="dense"
                id="title"
                name="title"
                label="Titulo"
                type="email"
                fullWidth
                variant="standard"
                onChange={handleChangeCategory}
            />
            <TextField
                margin="dense"
                id="description"
                name="description"
                label="Descripción"
                type="email"
                fullWidth
                variant="standard"
                onChange={handleChangeCategory}
            />
            </DialogContent>
            <DialogActions>
            <Button onClick={handleSaveCategory}>Guardar</Button>
            </DialogActions>
        </Dialog>
        
          <h1>Administración de plantillas</h1>
          <h3>Agregar plantillas</h3>
          
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={4} lg={3}>
                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '100%' },
                    }}
                    noValidate
                    autoComplete="off"
                    >
                    <TextField value={template.name} required id="name" name="name" label="Nombre" variant="outlined" size="small" onChange={handleChange}/>
                    <TextField value={template.description} required id="description" name="description" label="Descripción" variant="outlined" size="small" onChange={handleChange}/>
                    <TextField value={template.price} required id="price" name="price" label="Precio" variant="outlined" size="small" type="number" onChange={handleChange}/>
                </Box>
            </Grid>
            <Grid item xs={12} sm={12} md={4} lg={3}>
                <Box
                    sx={{
                        '& > :not(style)': { m: 0, width: '100%'},
                    }}
                    >
                        <img src={template.preview} width="100%" height="150" onClick={handleClickPreviewImage} style={{cursor: 'pointer'}}/>
                    <Button
                        variant="contained"
                        component="label"
                        >
                        Upload File
                        <input
                        multiple
                        ref={hiddenFileInput}
                        required
                        type="file"
                        hidden
                        accept=".jpg, .jpeg, .png"
                        onChange={handleSelectImage}
                        />
                  </Button>
                </Box>
            </Grid>
            
            <Grid item xs={12} sm={12} md={4} lg={3}>
                <Box
                    sx={{
                        '& > :not(style)': { my: 3, width: '100%'},
                    }}
                    >
                        
                    <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">Categoría</InputLabel>
                        <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        label="Categoría"
                        onChange={handleSelectCategory}
                        >
                        {categories.map((row) => (
                            <MenuItem value={row.id}>{row.title}</MenuItem>
                        ))}
                        </Select>
                    </FormControl>
                    <Stack direction="row" spacing={1}>
                    <Button
                        fullWidth
                        variant="outlined" color="info"
                        onClick={handleDialogCategoryOpen}
                        >
                        Nueva categoría
                  </Button>
                  <IconButton 
                        disabled={(template.category) ? false : true }
                        onClick={handleDeleteCategory} 
                        size="small" 
                        sx={{color:'#C00000'}} > 
                  <FontAwesomeIcon 
                        icon={faTrash} 
                        pull="right"/> 
                  </IconButton>
                  </Stack>
                </Box>
            </Grid>
            <Grid item xs={12} sm={12} md={4} lg={2}>
                <Box
                    sx={{
                        '& > :not(style)': { my: 3, width: '100%'},
                    }}
                    >
                    {!edit && (<Button
                        variant="contained" color="success"
                        onClick={handleSave}
                        >
                        Guardar
                  </Button>)}
                  {edit && (<Button
                        variant="contained"
                        onClick={handleSave}
                        >
                        Editar
                  </Button>)}
                    <Button
                        variant="contained" color="error"
                        onClick={handleCancel}
                        >
                        Cancelar
                  </Button>
                </Box>
            </Grid>
        </Grid>


          <h3>Plantillas existentes</h3>
          <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                  <TableHead>
                      <TableRow>
                          <TableCell>ID</TableCell>
                          <TableCell>Nombre</TableCell>
                          <TableCell>Descripción</TableCell>
                          <TableCell>Categoría</TableCell>
                          <TableCell>Precio</TableCell>
                          <TableCell>Ventas</TableCell>
                          <TableCell>Puntuación</TableCell>
                          <TableCell>Fecha de registro</TableCell>
                          <TableCell>Tools</TableCell>
                      </TableRow>
                  </TableHead>
                  <TableBody>
                  {templates.map((row) => (
                      <TableRow
                      key={row.name}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                      <TableCell component="th" scope="row">
                          {row.id}
                      </TableCell>
                      <TableCell>{row.name}</TableCell>
                      <TableCell>{row.description}</TableCell>
                      <TableCell>{(row.categoryId) ? row.category.title : <i>Sin categoría</i>}</TableCell>
                      <TableCell>{row.price}</TableCell>
                      <TableCell>{row.sales}</TableCell>
                      <TableCell>{row.score}</TableCell>
                      <TableCell>{row.createdAt.substr(0, 10)+" "+row.createdAt.substr(11, 5)}</TableCell>
                      <TableCell><Stack direction="row" spacing={1}><IconButton value={row.id} onClick={handleFill} size="small" sx={{color:'#1688C5'}}> <FontAwesomeIcon icon={faPencil} /> </IconButton> <IconButton value={row.id} onClick={handleDelete} size="small" sx={{color:'#C00000'}} ><FontAwesomeIcon icon={faTrash} /></IconButton></Stack></TableCell>
                      </TableRow>
                  ))}
                  {templates.length === 0 &&(
                      <TableRow>
                          <TableCell>
                              No hay resultados
                          </TableCell>
                      </TableRow>
                  )}
                  </TableBody>
              </Table>
          </TableContainer>
      </Paper>
    );
}

export default Template;