import { Button, FormControl, InputLabel, MenuItem, Select, TextField } from "@mui/material";
import Paper from "@mui/material/Paper";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Grid from '@mui/material/Grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Dialog from '@mui/material/Dialog';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import { faCheck, faPencil, faTrash, faXmark } from "@fortawesome/free-solid-svg-icons";

import axios from '../../config/axiosClient';
import { useState, useEffect, useRef } from "react";
import './styles.css';

const AdminUsers = () => {
    const [users, setUsers] = useState([]);  
    const [image, setImage] = useState([]);
    const [user, setUser] = useState({preview: 'https://fallofthewall25.com/img/default-user.jpg', active: true});
    const [edit, setEdit] = useState(false);
    const [title, setTitle] = useState('Registrar usuario');
    const [dialog, setDialog] = useState([{open: false, status: false, title: '', msg: ''}]);
    const hiddenFileInput = useRef(null);
      
    const allUsers = () => {
        axios.get(`http://localhost:8080/users/all`)
        .then((res) => {
            setUsers([...res.data])
            console.log(res);
        })
        .catch((error) => {
            console.log(error)
        });
    }  

    const handleCancel = (e) => {
        const emptyArray = {name: '', lastname: '', username : '', email: '', role: ''};
        setUser({...emptyArray, preview: 'https://fallofthewall25.com/img/default-user.jpg'});
        setImage([]);
        setTitle('Registrar usuario');
        setEdit(false);
    }

    const handleChange = (e) => {
        setUser({...user, [e.target.name]: e.target.value});
        console.log(user);
    }

    const handleClickPreviewImage = event => {
        hiddenFileInput.current.click();
      };

    const handleDelete = (e) => {
        axios.post(`http://localhost:8080/user/delete`, {id: e.currentTarget.value})
        .then((res) => {
            if(res.data.message == 'No autorizado para realizar esta acción.') {
                setDialog({open: true, status: false, msg: 'No tienes permisos'});
            } else {
                setDialog({open: true, status: true, msg: 'Usuario eliminado con éxito'});
            }
        })
        .catch(function (error) {
            if (error.response) {
                setDialog({open: true, status: true, title: 'Eliminado', msg: error.response.data});
            } 
        }); 
    }

    const handleDialogClose = () => {
        setDialog({...dialog, open: false});
    };

    const handleFill = (e) => {
        setEdit(true);
        setTitle('Editar usuario');
        axios.get(`http://localhost:8080/user?id=${e.currentTarget.value}`)
        .then((res) => {
            setUser({...res.data, preview: res.data.avatar, password: ''});
        })
        .catch((error) => {
            setDialog({open: true, status: false, title: 'Error', msg: error});
            setEdit(false);
            setTitle('Guardar usuario');
        });
    }

    const handleSelectImage = (e) => {
        setUser({...user, preview: URL.createObjectURL(e.target.files[0]), avatar: 'http://localhost/sai/assets/'+e.target.files[0].name});
        setImage(e.target.files[0]);
        console.log(user);
    }
    
    const handleSave = (e) => {
        if(user.name && user.lastname && user.username && user.role && user.avatar) {
            const formData = new FormData(); 
        
            // Update the formData object 
            formData.append( 
            "myFile", 
            image, 
            image.name
            ); 
            axios.post("http://localhost/sai/scripts/upload.php", formData).catch(function (error) {
                if (error.response) {
                    setDialog({open: true, status: false, title: 'Error', msg: error.response.data});
                }
            }); 
            if(!edit) {
                axios.post(`http://localhost:8080/signup`, user).catch(function (error) {
                    if (error.response) {
                        setDialog({open: true, status: false, title: 'Error', msg: error.response.data});
                    } 
                }); 

                setDialog({open: true, status: true, title: 'Guardado', msg: 'Nuevo usuario registrado con éxito'});
                setEdit(false);

            } else {
                axios.post(`http://localhost:8080/user/update`, user).catch(function (error) {
                    if (error.response) {
                        setDialog({open: true, status: false, title: 'Error', msg: error.response.data});
                    } 
                }); 

                setDialog({open: true, status: true, title: 'Guardad', msg: 'Cambios guardados con éxito'});
                setEdit(false);
            }
        } else {
            setDialog({open: true, status: false, title: 'Error', msg: 'Debes llenar todos los campos'});
            console.log(user);
        }
        
    }

    const handleSelectRole = (e) => {
        setUser({...user, role: e.target.value});
        console.log(user);
    }
      
    useEffect(() => {
        allUsers();
  }, [dialog.open]
)

    return (
        <Paper
        sx={{
          p: 3,
          minHeight: '75vh',
        }}
      >
          <Dialog
            open={dialog.open}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            >
            {!dialog.status && (<Alert variant="filled" severity="warning" 
            onClose={handleDialogClose}>  
            <AlertTitle>{dialog.title}</AlertTitle>
            {dialog.msg}
            </Alert>)}
            {dialog.status && (<Alert variant="filled" severity="success"
            onClose={handleDialogClose}>    
            <AlertTitle>{dialog.title}</AlertTitle>
            {dialog.msg}
            </Alert>)}
          </Dialog>

          <h1>Administración de usuarios</h1>
          <h3>{title}</h3>
          
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={4} lg={4}>
                <Box
                    component="form"
                    sx={{
                        '& > :not(style)': { m: 1, width: '100%' },
                    }}
                    noValidate
                    autoComplete="off"
                    >
                        <div className="cardMedia" onClick={handleClickPreviewImage}>
                            <img src={user.preview}/>
                        </div>
                        <Button
                            sx={{mt: 0}}
                            variant="contained"
                            component="label"
                            >
                            Cargar archivo
                            <input
                            ref={hiddenFileInput}
                            required
                            type="file"
                            hidden
                            accept="image/*"
                            onChange={handleSelectImage}
                            />
                        </Button>
                    <Stack direction="row" spacing={2}>
                        <TextField value={user.name} required id="name" name="name" label="Nombre" variant="outlined" size="small" onChange={handleChange}/>
                        <TextField value={user.lastname} required id="lastname" name="lastname" label="Apellido" variant="outlined" size="small" onChange={handleChange}/>
                    </Stack>
                    <Stack direction="row"  spacing={2}>
                        <TextField value={user.username} required id="username" name="username" label="Usuario" variant="outlined" size="small" onChange={handleChange}/>
                        <TextField value={user.email} disabled={(edit) ? true : false} required id="email" name="email" variant="outlined" size="small" type="email" onChange={handleChange}/>
                    </Stack>
                    
                    
                    <Stack direction="row"  spacing={2}>
                    <FormControl fullWidth size="small">
                        <InputLabel id="demo-simple-select-label">Elegir rol</InputLabel>
                        <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={(user.role) ? user.role : ''}
                        onChange={handleSelectRole}
                        >
                        <MenuItem value="super">Super</MenuItem>
                        <MenuItem value="admin">Administrador</MenuItem>
                        <MenuItem value="user">Usuario</MenuItem>
                        </Select>
                    </FormControl>
                        <TextField value={user.password} required id="password" name="password" variant="outlined" size="small" type="password" onChange={handleChange}/>
                    </Stack>
                    <Stack direction="row" spacing={2}>
                    {!edit && (<Button
                        fullWidth
                        variant="contained" color="success"
                        onClick={handleSave}
                        >
                        Guardar
                  </Button>)}
                  {edit && (<Button
                        fullWidth
                        variant="contained"
                        onClick={handleSave}
                        >
                        Editar
                  </Button>)}
                    <Button
                        fullWidth
                        variant="contained" color="error"
                        onClick={handleCancel}
                        >
                        Cancelar
                  </Button>
                  </Stack>
                </Box>
            </Grid>
            <Grid item xs={12} sm={12} md={8} lg={8}>
                <Box
                    sx={{
                        '& > :not(style)': { mx: 2, width: '100%'},
                    }}
                    >
                    <h3>Usuarios existentes</h3>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>ID</TableCell>
                                    <TableCell>Nombre</TableCell>
                                    <TableCell>Usuario</TableCell>
                                    <TableCell>Correo</TableCell>
                                    <TableCell>Rol</TableCell>
                                    <TableCell>Verificado</TableCell>
                                    <TableCell>Activo</TableCell>
                                    <TableCell>Fecha de registro</TableCell>
                                    <TableCell>Tools</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                            {users.map((row) => (
                                <TableRow
                                key={row.name}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell>{row.name}</TableCell>
                                <TableCell>{row.username}</TableCell>
                                <TableCell>{row.email}</TableCell>
                                <TableCell>{row.role}</TableCell>
                                <TableCell align="center">{(row.verified) ? <FontAwesomeIcon icon={faCheck} color="green"/> : <FontAwesomeIcon icon={faXmark} color="red"/>}</TableCell>
                                <TableCell align="center">{(row.active) ? <FontAwesomeIcon icon={faCheck} color="green"/> : <FontAwesomeIcon icon={faXmark} color="red"/>}</TableCell>
                                <TableCell>{row.createdAt.substr(0, 10)+" "+row.createdAt.substr(11, 5)}</TableCell>
                                <TableCell><Stack direction="row" spacing={1}><IconButton value={row.id} onClick={handleFill} size="small" sx={{color:'#1688C5'}}> <FontAwesomeIcon icon={faPencil} /> </IconButton> <IconButton value={row.id} onClick={handleDelete} size="small" sx={{color:'#C00000'}} ><FontAwesomeIcon icon={faTrash} /></IconButton></Stack></TableCell>
                                </TableRow>
                            ))}
                            {users.length === 0 &&(
                                <TableRow>
                                    <TableCell>
                                        No hay resultados
                                    </TableCell>
                                </TableRow>
                            )}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Box>
            </Grid>
        </Grid>
      </Paper>
    );
}

export default AdminUsers;