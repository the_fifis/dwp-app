import React from 'react';
import Paper from "@mui/material/Paper";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { Button, TextField } from "@mui/material";
import { CardActionArea } from '@mui/material';
import Grid from '@mui/material/Grid';
import Dialog from '@mui/material/Dialog';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Stack from '@mui/material/Stack';

import axiosClient from '../../config/axiosClient';
import { useState, useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencil, faTrash } from "@fortawesome/free-solid-svg-icons";

const Reports = () => {
  const [reports, setReports] = useState([]);    
  const [dialogReport, setDialogReport] = useState(false); 
  const [dialog, setDialog] = useState([{open: false, status: false, title: '', msg: ''}]);
  const [report, setReport] = useState([]);  
  
  const allReports = () => {
    axiosClient.get(`http://localhost:8080/reports`)
    .then((res) => {
      setReports([...res.data.resultados])
    })
    .catch((error) => {
        console.log(error)
    });
  }

  const handleChangeReport = (e) => {
    setReport({...report, [e.target.name]: e.target.value});
    console.log(report);
  }

      
  const handleDelete = (e) => {
    axiosClient.post(`http://localhost:8080/report/delete`, {id: e.currentTarget.value}).catch(function (error) {
        if (error.response) {
            setDialog({open: true, status: true, title: 'Eliminado', msg: error.response.data});
            allReports();
        } 
    }); 
    
    setDialog({open: true, status: true, msg: 'Reporte eliminado con éxito'});
}


  const handleDialogReportOpen = (e) => {
    setDialogReport(true);
    console.log(report);
  };

  const handleDialogReportClose = () => {
    setDialogReport(false);
  };

  const handleDialogClose = () => {
      setDialog({...dialog, open: false});
  };

  const handleEdit = (e) => {
    axiosClient.get(`http://localhost:8080/report?q=${e.currentTarget.value}`)
    .then((res) => {
        setReport({...res.data.resultados[0]});
        handleDialogReportOpen();
        allReports();
    })
    .catch((error) => {
        setDialog({open: true, status: false, title: 'Error', msg: error});
    });
}

  const handleSaveReport = (e) => {
    if(report.title && report.reason) {
      axiosClient.post(`http://localhost:8080/report/update`, {id: report.id, title: report.title, reason: report.reason})
        .then((res) => {
          setDialog({open: true, status: true, title: 'Guardado', msg: 'Cambios guardados con éxito'});
          setDialogReport(false);
        })
        .catch((error) => {
          setDialogReport(false);
          setDialog({open: true, status: false, msg: error.response});
          console.log(error.response);
        });
    } else {
        setDialog({open: true, status: false, title: 'Error', msg: 'Debes llenar todos los campos'});
    }
}

  useEffect(() => {
        allReports();
  }, []
)

  return (
    <Paper
      sx={{
        p: 3,
        minHeight: '75vh',
      }}
    >
          <Dialog
            open={dialog.open}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            >
            {!dialog.status && (<Alert variant="filled" severity="warning" 
            onClose={handleDialogClose}>  
            <AlertTitle>{dialog.title}</AlertTitle>
            {dialog.msg}
            </Alert>)}
            {dialog.status && (<Alert variant="filled" severity="success"
            onClose={handleDialogClose}>    
            <AlertTitle>{dialog.title}</AlertTitle>
            {dialog.msg}
            </Alert>)}
          </Dialog>

          <Dialog open={dialogReport} onClose={handleDialogReportClose}>
            <DialogTitle>Editar reporte del template {report.id}</DialogTitle>
            <DialogContent>
            <TextField
                autoFocus
                margin="dense"
                id="title"
                name="title"
                label="Titulo"
                type="text"
                fullWidth
                value={report.title}
                variant="standard"
                onChange={handleChangeReport}
            />
            <TextField
                margin="dense"
                id="reason"
                name="reason"
                label="Razón"
                type="text"
                fullWidth
                value={report.reason}
                variant="standard"
                onChange={handleChangeReport}
            />
            </DialogContent>
            <DialogActions>
            <Button onClick={handleSaveReport}>Guardar</Button>
            </DialogActions>
        </Dialog>
      <h2>Mis reportes</h2>
      <Grid container spacing={2} 
              justifyContent="center">
                
          <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                  <TableHead>
                      <TableRow>
                          <TableCell>ID</TableCell>
                          <TableCell>Título</TableCell>
                          <TableCell>Razón</TableCell>
                          <TableCell>Plantilla</TableCell>
                          <TableCell>Fecha</TableCell>
                          <TableCell>Tools</TableCell>
                      </TableRow>
                  </TableHead>
                  <TableBody>
                  {reports.map((row) => (
                      <TableRow
                      key={row.name}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                      >
                      <TableCell component="th" scope="row">
                          {row.id}
                      </TableCell>
                      <TableCell>{row.title}</TableCell>
                      <TableCell>{row.reason}</TableCell>
                      <TableCell>{row.template_id}</TableCell>
                      <TableCell>{(row.createdAt > row.updatedAt) ? row.createdAt.substr(0, 10)+" "+row.createdAt.substr(11, 5) : row.updatedAt.substr(0, 10)+" "+row.updatedAt.substr(11, 5)}</TableCell>
                      <TableCell><Stack direction="row" spacing={1}><IconButton value={row.id} onClick={handleEdit} size="small" sx={{color:'#1688C5'}}> <FontAwesomeIcon icon={faPencil} /> </IconButton> <IconButton value={row.id} onClick={handleDelete} size="small" sx={{color:'#C00000'}} ><FontAwesomeIcon icon={faTrash} /></IconButton></Stack></TableCell>
                      </TableRow>
                  ))}
                  {reports.length === 0 &&(
                      <TableRow>
                          <TableCell>
                              No hay resultados
                          </TableCell>
                      </TableRow>
                  )}
                  </TableBody>
              </Table>
          </TableContainer>
      </Grid>
    </Paper>
  );
}

export default Reports;
