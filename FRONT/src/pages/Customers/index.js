import { Button, TextField } from "@mui/material";
import Paper from "@mui/material/Paper";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import FormControlLabel from '@mui/material/FormControlLabel';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import FormControl from '@mui/material/FormControl';
import Checkbox from '@mui/material/Checkbox';
import Grid from '@mui/material/Grid';

import axios from '../../config/axiosClient';
import { useState, useEffect } from "react";

const Customer = () => {
    const [query, setQuery] = useState("");
    const [category, setCategory] = useState('');
    const [customers, setCustomers] = useState([]);
    const [checked, setChecked] = useState(true);

    const allCustomers = () => {
        axios.get(`http://localhost:8080/customers`)
        .then((res) => {
            setCustomers([...res.data.resultados])
        })
        .catch((error) => {
            console.log(error)
        });
    }

    const filtrar = () => {
        axios.post(`http://localhost:8080/filtrar`, {category: category})
        .then((res) => {
            setCustomers([...res.data.resultados]);
        })
        .catch((error) => {
            console.log(error)
        });  
    };

    const handleChange = (e) => {
        setQuery(e.target.value);
        if(e.target.value == '') {
            allCustomers();
        } else {
            searchCust();
        }
    };

    const handleFilter = (e) => {
        setCategory(e.target.value);
    };
    
    const searchCust = () => {
        if(query) {
            axios.get(`http://localhost:8080/buscar?q=${query}`)
            .then((res) => {
                setCustomers([...res.data.resultados])
                if(res.data.length === 0) {
                    alert('No hay coincidencias');
                }
            })
            .catch((error) => {
                console.log(error)
            });
        } else {
            allCustomers();
        }
    }

    const handlecheck = () => {
        setChecked(!checked);
        todayCustomers();
    }

    const todayCustomers = () => {
        if(checked) {
            axios.get(`http://localhost:8080/recent`)
            .then((res) => {
                setCustomers([...res.data.resultados])
            })
            .catch((error) => {
                console.log(error)
            });
        } else {
            allCustomers();
        }
    }

    useEffect(() => {
            if(category) {
                filtrar();
            } else {
                if(customers.length === 0) { 
                allCustomers();
            }
        }
        }, [category]
    )

    return (
        <Paper
          sx={{
            p: 3,
            minHeight: '75vh',
          }}
        >
            <h1>Customers</h1>


            <Grid container spacing={2}>
                <Grid item xs={3}>
                    <TextField id="standard-basic" label="Búsqueda" variant="standard" 
                    value={query}
                    onChange={handleChange}
                    fullWidth></TextField>
                </Grid>
                <Grid item xs={4}>
                    <Button variant="contained"
                    onClick={searchCust}>Buscar</Button>
                    <FormControlLabel 
                    control={<Checkbox onChange={handlecheck}/>} 
                    label="Registros de hoy" 
                    labelPlacement="start"/>
                </Grid>
                <Grid item xs={5}>
                    <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">Categoría</InputLabel>
                        <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={category}
                        label="Categoría"
                        onChange={handleFilter}
                        >
                        <MenuItem value={1}>Nuevo Cliente</MenuItem>
                        <MenuItem value={2}>Frecuente</MenuItem>
                        <MenuItem value={3}>VIP</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>


            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell align="right">Nombre</TableCell>
                            <TableCell align="right">Correo</TableCell>
                            <TableCell align="right">Teléfono</TableCell>
                            <TableCell align="right">Categoría</TableCell>
                            <TableCell align="right">Fecha de registro</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {customers.map((row) => (
                        <TableRow
                        key={row.name}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                        <TableCell component="th" scope="row">
                            {row.id}
                        </TableCell>
                        <TableCell align="right">{row.name}</TableCell>
                        <TableCell align="right">{row.email}</TableCell>
                        <TableCell align="right">{row.phone}</TableCell>
                        <TableCell align="right">{row.category && row.category.name || 'Sin categoría'}</TableCell>
                        <TableCell align="right">{row.createdAt}</TableCell>
                        </TableRow>
                    ))}
                    {customers.length === 0 &&(
                        <TableRow>
                            <TableCell>
                                No hay resultados
                            </TableCell>
                        </TableRow>
                    )}
                    </TableBody>
                </Table>
            </TableContainer>
        </Paper>
    );
};

export default Customer;