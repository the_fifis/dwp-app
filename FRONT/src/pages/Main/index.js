import React from 'react';
import Paper from "@mui/material/Paper";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { Button, TextField } from "@mui/material";
import { CardActionArea } from '@mui/material';
import Grid from '@mui/material/Grid';
import Dialog from '@mui/material/Dialog';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

import axiosClient from '../../config/axiosClient';
import { useState, useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar, faInfoCircle } from '@fortawesome/free-solid-svg-icons';

const Main = () => {
  const [templates, setTemplates] = useState([]);    
  const [best, setBest] = useState([]);   
  const [dialogReport, setDialogReport] = useState(false); 
  const [dialog, setDialog] = useState([{open: false, status: false, title: '', msg: ''}]);
  const [report, setReport] = useState([]);  
  
  const allTemplates = () => {
    axiosClient.get(`http://localhost:8080/templates/recents`)
    .then((res) => {
      setTemplates([...res.data.resultados])
    })
    .catch((error) => {
        console.log(error)
    });
  }
  
  const bestTemplate = () => {
    axiosClient.get(`http://localhost:8080/templates/top`)
    .then((res) => {
      setBest([...res.data.resultados])
    })
    .catch((error) => {
        console.log(error)
    });
  }

  const handleChangeReport = (e) => {
    setReport({...report, [e.target.name]: e.target.value});
    console.log(report);
  }

  const handleDialogReportOpen = (e) => {
    setReport({...report, template_id: e.currentTarget.value});
    setDialogReport(true);
    console.log(e.currentTarget.value);
  };

  const handleDialogReportClose = () => {
    setDialogReport(false);
  };

  const handleDialogClose = () => {
      setDialog({...dialog, open: false});
  };

  const handleSaveReport = (e) => {
    if(report.title && report.reason) {
      axiosClient.post(`http://localhost:8080/report/add`, report)
        .then((res) => {
          setDialog({open: true, status: true, title: 'Guardado', msg: 'Cambios guardados con éxito'});
          setDialogReport(false);
        })
        .catch((error) => {
          setDialogReport(false);
          setDialog({open: true, status: false, msg: error.response});
          console.log(error.response);
        });
    } else {
        setDialog({open: true, status: false, title: 'Error', msg: 'Debes llenar todos los campos'});
    }
}

  useEffect(() => {
        bestTemplate();
        allTemplates();
  }, []
)

  return (
    <Paper
      sx={{
        p: 3,
        minHeight: '75vh',
      }}
    >
          <Dialog
            open={dialog.open}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            >
            {!dialog.status && (<Alert variant="filled" severity="warning" 
            onClose={handleDialogClose}>  
            <AlertTitle>{dialog.title}</AlertTitle>
            {dialog.msg}
            </Alert>)}
            {dialog.status && (<Alert variant="filled" severity="success"
            onClose={handleDialogClose}>    
            <AlertTitle>{dialog.title}</AlertTitle>
            {dialog.msg}
            </Alert>)}
          </Dialog>

          <Dialog open={dialogReport} onClose={handleDialogReportClose}>
            <DialogTitle>Crear reporte del template</DialogTitle>
            <DialogContent>
            <DialogContentText>
                Agrega un título y razón del reporte.
            </DialogContentText>
            <TextField
                autoFocus
                margin="dense"
                id="title"
                name="title"
                label="Titulo"
                type="text"
                fullWidth
                variant="standard"
                onChange={handleChangeReport}
            />
            <TextField
                margin="dense"
                id="reason"
                name="reason"
                label="Razón"
                type="text"
                fullWidth
                variant="standard"
                onChange={handleChangeReport}
            />
            </DialogContent>
            <DialogActions>
            <Button onClick={handleSaveReport}>Guardar</Button>
            </DialogActions>
        </Dialog>
      <h2>Lo mas visto</h2>
      <Grid container spacing={2} 
              justifyContent="center">
        <Grid item xs={10} >
          {best.map((row) => (
            <Card >
              <CardActionArea>
                  <CardMedia
                    component="img"
                    height="340"
                    image={row.image}
                    alt="template image"
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {row.name}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      {row.description}
                    <FontAwesomeIcon icon={faStar} pull="right" size='xl' color='#FFC000' beat />
                    </Typography>
                  </CardContent>
                </CardActionArea>
            </Card>
          ))}
        </Grid>
      </Grid>

      <h2>Lo mas reciente</h2>
      <Grid container spacing={2} >
        {templates.map((row) => (
          <Grid item xs={12} sm={12} md={4} lg={3} >
              <Card >
                <CardActionArea>
                    <CardMedia
                      component="img"
                      height="140"
                      image={row.image}
                      alt="template image"
                    />
                    <CardContent>
                      <Typography gutterBottom variant="h6" component="div">
                        {row.name}
                      </Typography>
                      <Typography variant="body2" color="text.secondary">
                        {row.description}
                      </Typography>
                    <IconButton 
                          onClick={handleDialogReportOpen}
                          value={row.id}
                          size="small" 
                          sx={{color:'#C00000'}} > 
                    <FontAwesomeIcon 
                          icon={faInfoCircle} 
                          pull="right"/> 
                    </IconButton>
                    </CardContent>
                  </CardActionArea>
              </Card>
          </Grid>
        ))}
      </Grid>
    </Paper>
  );
}

export default Main;
