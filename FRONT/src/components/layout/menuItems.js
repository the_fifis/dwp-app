
const items = [
  {
    id: 'm-1',
    text: 'Inicio',
    icon: 'faHome',
    url: '/',
    denyFor: ['none'],
  },
  {
    id: 'm-1',
    text: 'Todos los reportes',
    icon: 'faListAlt',
    url: '/reports',
    denyFor: ['user'],
  },
];

export default items;
