import React from 'react';
import { Route, Routes } from 'react-router-dom';

import Layout from '../components/layout';
import PrivateRoute from './PrivateRoute';
import Main from '../pages/Main';
import Login from '../pages/Auth';
import ForgotPassword from '../pages/Auth/ForgotPassword';
import NewPassword from '../pages/Auth/NewPassword';

import Customer from '../pages/Customers';
import AdminTemplates from '../pages/Templates';
import AdminUsers from '../pages/AdminUsers';
import Reports from '../pages/Reports';


const Router = () => (
  <Routes>
    <Route path="/login" element={<Login />} />
    <Route path="/forgot-password" element={<ForgotPassword />} />
    <Route path="/restablecer-contrasena/:token" element={<NewPassword />} />
    <Route element={<Layout />}>
      <Route path="/" element={<Main />} />
      <Route path="/customers" element={<PrivateRoute><Customer /></PrivateRoute>}/>
      <Route path="/admin-templates" element={<PrivateRoute><AdminTemplates /></PrivateRoute>}/>
      <Route path="/admin-users" element={<PrivateRoute><AdminUsers /></PrivateRoute>}/>
      <Route path="/reports" element={<PrivateRoute><Reports /></PrivateRoute>}/>
    </Route>
  </Routes>
);

// <Route path="/personal" element={<PrivateRoute><Employees /></PrivateRoute>}/>

export default Router;
