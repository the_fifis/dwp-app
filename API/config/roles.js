const AccessControl = require('accesscontrol');

const ac = new AccessControl();

/**
 * definir roles del más inferior al superior
 *  ninguno
 *    usuario
 *      admin
 *        super
 */
exports.roles = () => {
  ac.grant('none');
  // aqui los permisos de rol: ninguno

  ac.grant('user')
    .readOwn('perfil')
    .readAny(['template', 'category'])
    .createAny('report');

  ac.grant('admin')
    .extend('user') // heredar rol
    .readAny(['user', 'report']) // poder leer usuarios
    .createAny(['template', 'category', 'user', 'report'])
    .updateAny(['template', 'category', 'user', 'report'])
    .deleteAny(['report']);
  
  ac.grant('super')
    .extend('admin')
    .deleteAny(['template', 'category', 'user']);
  
    return ac;
};