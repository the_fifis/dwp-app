const { TemplateCategory } = require('../models');
const { Op } = require('sequelize');

exports.add = async (req, res, next) => {
  try {
    
    const category = await TemplateCategory.create({
      title: req.body.title,
      description: req.body.description,
      logo: req.body.logo
    });
    res.json({resultados: category.id});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    
    const category = await TemplateCategory.destroy({
      where: {
      id: req.body.id
      }
    });
    res.json({resultados: category.id});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.get = async (req, res, next) => {
  try {
    
    const category = await TemplateCategory.findAll({
      where: {
        id: req.query.q
      }
    });
    res.json({resultados: category});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.list = async (req, res, next) => {
  try {
    
    const category = await TemplateCategory.findAll();
    res.json({resultados: category});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.update = async (req, res, next) => {
  try {
    
    const category = await TemplateCategory.update({
      title: req.body.title,
      description: req.body.description,
      logo: req.body.logo
    }, {
      where: {
        id: req.body.id
      }
    });
    res.json({resultados: category});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

