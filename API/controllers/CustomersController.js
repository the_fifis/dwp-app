const { Customer } = require('../models');
const { Op } = require('sequelize');

exports.list = async (req, res, next) => {
  try {
    
    const customers = await Customer.findAll({
      include: ['category'],
    });
    res.json({resultados: customers});

  } catch(error) {
    res.status(500).json({
      message: 'Error al leer clientes',
    });
  }
};

exports.filtrar = async (req, res, next) => {
  try {
    
    const customers = await Customer.findAll({
      where: {
        categoryId: req.body.category,
      },
      include: ['category'],
    });
    res.json({resultados: customers});

  } catch(error) {
    res.status(500).json({
      message: 'Error al leer clientes',
    });
  }
};

exports.buscar = async (req, res, next) => {
  try {
    console.log(req.query);
    const customers = await Customer.findAll({
      where: {
        [Op.or]: [
          {
            name: {
              [Op.like]: `%${req.query.q.toLowerCase()}%`
            }
          },
          {
            email: {
              [Op.like]: `%${req.query.q.toLowerCase()}%`
            }
          },
          {
            phone: {
              [Op.like]: `%${req.query.q.toLowerCase()}%`
            }
          }
        ]
      },
      include: ['category']
    });
    res.json({resultados: customers});
  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.recent = async (req, res, next) => {
  try {
    const d = new Date();
    const today = "".concat(d.getFullYear(), '-', (d.getMonth()+1).toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false}), '-', d.getDate());
    const customers = await Customer.findAll({
      where: {
        createdAt: {
          [Op.gte]: today
        }
      },
      include: ['category']
    });
    res.json({resultados: customers});
  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};