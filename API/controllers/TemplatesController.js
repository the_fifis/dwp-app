const { Templates } = require('../models');
const { Op } = require('sequelize');

exports.add = async (req, res, next) => {
  try {
    
    const template = await Templates.create({
      name: req.body.name,
      description: req.body.description,
      image: req.body.image,
      price: req.body.price,
      categoryId: req.body.category
    });
    res.json({resultados: template.id});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    
    const template = await Templates.destroy({
      where: {
      id: req.body.id
      }
    });
    res.json({resultados: template.id});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.get = async (req, res, next) => {
  try {
    
    const templates = await Templates.findAll({
      where: {
        id: req.query.q
      },
      include: ['category']
    });
    res.json({resultados: templates});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.list = async (req, res, next) => {
  try {
    
    const templates = await Templates.findAll({include: ['category']});
    res.json({resultados: templates});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.best = async (req, res, next) => {
  try {
    
    const templates = await Templates.findAll({
      limit: 1,
      order: [
        ['score', 'DESC']
      ],
      include: ['category']
    });
    res.json({resultados: templates});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.recent = async (req, res, next) => {
  try {
    const d = new Date();
    const templates = await Templates.findAll({
      order: [
        ['createdAt', 'DESC']
      ],
      limit: 4,
      include: ['category']
    });
    res.json({resultados: templates});
  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.update = async (req, res, next) => {
  try {
    
    const template = await Templates.update({
      name: req.body.name,
      description: req.body.description,
      image: req.body.image,
      price: req.body.price,
      categoryId: req.body.category
    }, {
      where: {
        id: req.body.id
      }
    });
    res.json({resultados: template});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.buscar = async (req, res, next) => {
  try {
    console.log(req.query);
    const templates = await Templates.findAll({
      where: {
        [Op.or]: [
          {
            name: {
              [Op.like]: `%${req.query.q.toLowerCase()}%`
            }
          },
          {
            email: {
              [Op.like]: `%${req.query.q.toLowerCase()}%`
            }
          },
          {
            phone: {
              [Op.like]: `%${req.query.q.toLowerCase()}%`
            }
          }
        ]
      },
      include: ['category']
    });
    res.json({resultados: customers});
  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};
