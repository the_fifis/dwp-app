const { Reports } = require('../models');
const { Op } = require('sequelize');

exports.add = async (req, res, next) => {
  try {
    const reportData = {...req.body};
    
    const report = await Reports.create(reportData);
    res.json({resultados: report.id});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    
    const report = await Reports.destroy({
      where: {
      id: req.body.id
      }
    });
    res.json({resultados: report.id});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.get = async (req, res, next) => {
  try {
    
    const report = await Reports.findAll({
      where: {
        id: req.query.q
      }
    });
    res.json({resultados: report});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.list = async (req, res, next) => {
  try {
    
    const report = await Reports.findAll();
    res.json({resultados: report});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

exports.update = async (req, res, next) => {
  try {
    
    const reportData = {...req.body};
    const report = await Reports.update(reportData, {
      where: {
        id: req.body.id
      }
    });
    res.json({resultados: report});

  } catch(error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

