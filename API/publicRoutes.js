const express = require('express');

const router = express.Router();

const sesionController = require('./controllers/SesionController');
const usuariosController = require('./controllers/UsersController');
const passwordController = require('./controllers/PasswordController');

module.exports = function() {
  // rutas que no requieren autenticacion
  router.post('/login', sesionController.login);
  router.post('/signup', usuariosController.add);
  router.get('/users/all', usuariosController.listar);
  router.get('/user', usuariosController.mostrar);
  router.post('/user/update', usuariosController.update);
  router.post('/user/delete', usuariosController.delete);
  router.post('/recuperar-password', passwordController.resetPassword);
  router.post('/validar-token', passwordController.validarToken);
  router.post('/actualizar-password', passwordController.saveNewPassword);

  return router;
};