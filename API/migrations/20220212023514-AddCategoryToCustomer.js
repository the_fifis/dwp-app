'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn('Customers', 'categoryId',
      {
        //especificar el tipo de dato del nuevo campo
        type: Sequelize.INTEGER,
        references: { 
          model:'Categories', 
          key: 'id',
        },
        onUpdate: 'CASCADE', 
        onDelete: 'SET NULL',
        defaultValue: null,
        after: 'phone',
      }),
    ]);
  },

  async down (queryInterface, Sequelize) {
    return Promise.all ([
      queryInterface.removeColumn('Customers', 'categoryId')
    ]);
  }
};
