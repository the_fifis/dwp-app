'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn('Templates', 'categoryId',
      {
        //especificar el tipo de dato del nuevo campo
        type: Sequelize.INTEGER,
        references: { 
          model:'TemplateCategories', 
          key: 'id',
        },
        onUpdate: 'CASCADE', 
        onDelete: 'SET NULL',
        defaultValue: null,
        after: 'score',
      }),
    ]);
  },
  async down(queryInterface, Sequelize) {
    return Promise.all ([
      queryInterface.removeColumn('Templates', 'categoryId')
    ]);
  }
};