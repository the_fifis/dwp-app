'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn('Users', 'active',
      {
        //especificar el tipo de dato del nuevo campo
        type: Sequelize.BOOLEAN,
        onUpdate: 'CASCADE', 
        onDelete: 'SET NULL',
        defaultValue: null,
        after: 'verified',
      }),
      queryInterface.addColumn('Users', 'passwordResetToken',
      {
        //especificar el tipo de dato del nuevo campo
        type: Sequelize.STRING,
        onUpdate: 'CASCADE', 
        onDelete: 'SET NULL',
        defaultValue: null,
        after: 'active',
      }),
      queryInterface.addColumn('Users', 'passwordResetExpire',
      {
        //especificar el tipo de dato del nuevo campo
        type: Sequelize.DATE,
        onUpdate: 'CASCADE', 
        onDelete: 'SET NULL',
        defaultValue: null,
        after: 'passwordResetToken',
      }),
    ]);
  },

  async down (queryInterface, Sequelize) {
    return Promise.all ([
      queryInterface.removeColumn('Users', 'active'),
      queryInterface.removeColumn('Users', 'passwordResetToken'),
      queryInterface.removeColumn('Users', 'passwordResetExpire')
    ]);
  }
};
