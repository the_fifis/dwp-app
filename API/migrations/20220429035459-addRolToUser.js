'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return Promise.all([
      queryInterface.addColumn('Users', 'role',
      {
        //especificar el tipo de dato del nuevo campo
        type: Sequelize.STRING(16),
        onUpdate: 'CASCADE', 
        allowNull: false,
        defaultValue: 'user',
        after: 'email',
      })
    ]);
  },

  async down (queryInterface, Sequelize) {
    return Promise.all ([
      queryInterface.removeColumn('Users', 'role')
    ]);
  }
};
