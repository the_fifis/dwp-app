const express = require('express');
const router = express.Router();
const { grantAccess } = require('./middlewares/accessControl');

// importar los controladores
const customersController = require('./controllers/CustomersController');
const templatesController = require('./controllers/TemplatesController');
const categoriesController = require('./controllers/categoriesController');
const reportsController = require('./controllers/reportsController');

module.exports = () => {

  // definir las rutas

  // clientes
  router.get('/customers', grantAccess('readAny', 'user'), customersController.list);
  router.get('/buscar', grantAccess('readAny', 'user'), customersController.buscar);
  router.get('/recent', grantAccess('readAny', 'user'), customersController.recent);
  router.post('/filtrar', grantAccess('readAny', 'user'), customersController.filtrar);

  // templates
  router.get('/templates', grantAccess('readAny', 'template'), templatesController.list);
  router.post('/templates', grantAccess('createAny', 'template'), templatesController.add);
  router.post('/template/remove', grantAccess('deleteAny', 'template'), templatesController.delete);
  router.get('/templates/top', grantAccess('readAny', 'template'), templatesController.best);
  router.get('/template', grantAccess('readAny', 'template'), templatesController.get);
  router.post('/template/update', grantAccess('updateAny', 'template'), templatesController.update);
  router.get('/templates/recents', grantAccess('readAny', 'template'), templatesController.recent);

  // Categories
  router.get('/categories', grantAccess('readAny', 'category'), categoriesController.list);
  router.post('/category/add', grantAccess('createAny', 'category'), categoriesController.add);
  router.post('/category/delete', grantAccess('deleteAny', 'category'), categoriesController.delete);

  router.get('/reports', grantAccess('readAny', 'report'), reportsController.list);
  router.post('/report/add', grantAccess('createAny', 'report'), reportsController.add);
  router.post('/report/update', grantAccess('createAny', 'report'), reportsController.update);
  router.post('/report/delete', grantAccess('deleteAny', 'report'), reportsController.delete);
  router.get('/report', grantAccess('readAny', 'report'), reportsController.get);

  return router;
};
