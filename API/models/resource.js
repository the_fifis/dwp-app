'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Resource extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Resource.belongsTo(models.Templates, {
        as: 'template',
        foreignKey: 'templateId',
      });
    }
  }
  Resource.init({
    type: DataTypes.STRING,
    path: DataTypes.STRING,
    templateId: DataTypes.NUMBER,
    main: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Resource',
  });
  return Resource;
};