'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Templates extends Model {
    static associate(models) {
      Templates.belongsTo(models.TemplateCategory, {
        as: 'category',
        foreignKey: 'categoryId',
      });
      Templates.hasMany(models.Resource, {
        as: 'resources'
      });
    }
  }
  Templates.init({
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    image: DataTypes.STRING,
    price: DataTypes.INTEGER,
    sales: DataTypes.INTEGER,
    score: DataTypes.INTEGER,
    categoryId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Templates',
  });
  return Templates;
};