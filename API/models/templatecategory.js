'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TemplateCategory extends Model {
    static associate(models) {
      TemplateCategory.hasMany(models.Templates, {
        as: 'templates',
        foreignKey: 'categoryId'
      });
    }
  }
  TemplateCategory.init({
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    logo: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'TemplateCategory',
  });
  return TemplateCategory;
};