'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Reports extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    
     static associate(models) {
      Reports.belongsTo(models.Templates, {
        as: 'template',
        foreignKey: 'template_id',
      });
    }
  }
  Reports.init({
    title: DataTypes.STRING,
    reason: DataTypes.STRING,
    template_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Reports',
  });
  return Reports;
};